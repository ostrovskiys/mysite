from django import forms
from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField

from wagtail.snippets.models import register_snippet
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

class TextCategoryPage(Page):
    name = models.CharField(max_length=50)
    cont = models.TextField(null=True, blank=True)

    

    content_panels = Page.content_panels + [
        FieldPanel('name', classname="full"),
        FieldPanel('cont', classname="full"),
        
    ]


    def get_context(self, request):
        context = super().get_context(request)

        text_entries = TextPage.objects.child_of(self).live()
        context['text_entries'] = text_entries
    
        return context


class NewsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(role='News')

class ArticlesManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(role='Articles')

class TextPage(Page):
    intro = models.CharField(max_length=250)
    desc = RichTextField(blank=True)
    date = models.DateField("Post date")
    role = models.CharField(max_length=20, default='News')
    
    
    

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('desc'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
        FieldPanel('desc', classname="full"),
        FieldPanel('date'),
        FieldPanel('role'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]

class TextPageGalleryImage(Orderable):
    page = ParentalKey(TextPage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]



    
    