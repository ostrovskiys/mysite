# Generated by Django 2.2.7 on 2020-02-07 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0011_auto_20191128_0354'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='textpage',
            managers=[
            ],
        ),
        migrations.AlterField(
            model_name='textpage',
            name='role',
            field=models.CharField(default='News', max_length=20),
        ),
    ]
