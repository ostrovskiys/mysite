# Generated by Django 2.2.7 on 2019-11-14 03:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0003_auto_20191112_0429'),
    ]

    operations = [
        migrations.AddField(
            model_name='textpage',
            name='type_text',
            field=models.BooleanField(blank=True, default=True),
        ),
    ]
