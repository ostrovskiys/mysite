# Generated by Django 2.2.7 on 2019-11-14 03:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_auto_20191114_0332'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='textpage',
            name='count',
        ),
    ]
