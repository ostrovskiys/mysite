from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from content.models import TextCategoryPage
from content.models import TextPage


class HomePage(Page):
    body = RichTextField(blank=True)
    banner_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name="+"
    )
    banner_cta = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+"
    )

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        ImageChooserPanel("banner_image"),
        PageChooserPanel("banner_cta")
    ]

    
        

    
    def get_context(self, request):
        context = super().get_context(request)

        cnews = TextCategoryPage.objects.child_of(self).live().get(name='Новости спорта')
        news = TextPage.objects.child_of(cnews).live()
        latest_news = news[:10]
        

        context['cnews'] = cnews
        context['latest_news'] = latest_news
    
        return context
    
    
    
   